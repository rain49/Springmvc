package com.wiley.zookeeper;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.CountDownLatch;

/**
 * @ClassName DistributedLockByCurator
 * @Description curator实现分布式锁
 * @Author Rui.Wen
 * @Date 2019-11-08 16:07
 * @Version 1.0
 */
@Service
public class DistributedLockByCurator implements InitializingBean {

    private static Logger logger = LoggerFactory.getLogger(DistributedLockByCurator.class);

    private final static String TEST_PATH_LOCK = "testLock";

    private final static Integer THREAD_NUM = 1;

    private CountDownLatch countDownLatch = new CountDownLatch(THREAD_NUM);

    @Autowired
    private CuratorFramework curatorFramework;

    /**
     * 获取分布式锁
     *
     * @param path 节点路径
     */
    public void acquireLock(String path) {
        String keyPath = "/" + TEST_PATH_LOCK + "/" + path;
        while (true) {
            try {
                curatorFramework.create()
                        .creatingParentsIfNeeded()
                        .withMode(CreateMode.EPHEMERAL)
                        .withACL(ZooDefs.Ids.OPEN_ACL_UNSAFE)
                        .forPath(keyPath);
                logger.info(Thread.currentThread().getName() + "成功获取到锁，path：{}", keyPath);
                break;
            } catch (Exception e) {
                try {
                    //如果没有获取到锁,需要重新设置同步资源值
                    if (countDownLatch.getCount() <= 0) {
                        countDownLatch = new CountDownLatch(THREAD_NUM);
                    }
                    logger.info("未获取到锁，等待他人释放锁... path：{}", keyPath);
                    countDownLatch.await();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * 释放分布式锁
     *
     * @param path 节点路径
     * @return
     */
    public boolean releaseLock(String path) {
        String keyPath = "/" + TEST_PATH_LOCK + "/" + path;

        try {
            if (curatorFramework.checkExists().forPath(keyPath) != null) {
                curatorFramework.delete().forPath(keyPath);
            }
            logger.info("释放锁成功，path：{}", keyPath);
        } catch (Exception e) {
            logger.error("释放锁失败！！！");
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public void addWatcher(String path) throws Exception {
        String keyPath;
        if (path.equals(TEST_PATH_LOCK)) {
            keyPath = "/" + path;
        } else {
            keyPath = "/" + TEST_PATH_LOCK + "/" + path;
        }

        final PathChildrenCache childrenCache = new PathChildrenCache(curatorFramework, keyPath, true);
        //启动,启动策略有三种:同步,异步提交,异步 用的比较多的就是下面这种到
        childrenCache.start(PathChildrenCache.StartMode.POST_INITIALIZED_EVENT);
        childrenCache.getListenable().addListener((client, event) -> {
            if (event.getType().equals(PathChildrenCacheEvent.Type.CHILD_REMOVED)) {
                String oldPath = event.getData().getPath();
                logger.info("上一个会话已释放锁或已断开...节点路径为: " + oldPath);
                if (oldPath.contains(path)) {
                    countDownLatch.countDown();
                }
            }
        });
    }

    @Override
    public void afterPropertiesSet() {
        String path = "/" + TEST_PATH_LOCK;

        try {
            if (curatorFramework.checkExists().forPath(path) == null) {
                curatorFramework.create()
                        .creatingParentsIfNeeded()
                        .withMode(CreateMode.PERSISTENT)
                        .withACL(ZooDefs.Ids.OPEN_ACL_UNSAFE)
                        .forPath(path);
            }
            // 注册节点监听事件
            addWatcher(TEST_PATH_LOCK);
            logger.info("注册节点{}监听", TEST_PATH_LOCK);
        } catch (Exception e) {
            logger.error("连接zookeeper失败，>> {}", e.getMessage(), e);
        }
    }
}
