package com.wiley.zookeeper;

import org.apache.zookeeper.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @ClassName ZkWatch
 * @Description zookeeper监听器
 * @Author Rui.Wen
 * @Date 2019-07-22 14:57
 * @Version 1.0
 */
@Component
public class ZkWatch implements Watcher {
    private static Logger logger = LoggerFactory.getLogger(ZkWatch.class);

    String zooDataPath = "/name";
    ZooKeeper zooKeeper;

    public ZkWatch() {
        String hostPort = "10.86.216.178:2181";
        try {
            zooKeeper = new ZooKeeper(hostPort, 2000, this);
            if (zooKeeper.exists(zooDataPath, this) == null) {
                zooKeeper.create(zooDataPath, "".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void process(WatchedEvent watchedEvent) {
        System.out.printf("\nReceive Events: %s\n", watchedEvent.toString());
        if (watchedEvent.getType() == Event.EventType.NodeDataChanged) {
            try {
                byte[] data = zooKeeper.getData(zooDataPath, this, null);
                logger.info("Current Data: {} {}", zooDataPath, new String(data));
            } catch (KeeperException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
