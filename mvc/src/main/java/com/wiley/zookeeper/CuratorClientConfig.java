package com.wiley.zookeeper;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName ZookeeperConfig
 * @Description curator连接配置
 * @Author Rui.Wen
 * @Date 2019-07-23 13:41
 * @Version 1.0
 */
@Configuration
public class CuratorClientConfig {

    private static Logger logger = LoggerFactory.getLogger(CuratorClientConfig.class);

    @Autowired
    private MyCuratorWatcher myCuratorWatcher;

    private static final String ZOOKEEPER_HOST = "10.86.216.179:2181,10.86.216.182:2181,10.86.216.183:2181";

    private static final String ZK_PATH ="/trip";

    /**
     * 获取 CuratorFramework
     * 使用 curator 操作 zookeeper
     *
     * @return CuratorFramework
     */
    @Bean
    public CuratorFramework curatorFramework() throws Exception {
        //配置zookeeper连接的重试策略
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 5);
        //构建 CuratorFramework
        CuratorFramework curatorFramework = CuratorFrameworkFactory.builder()
                .connectString(ZOOKEEPER_HOST)
                .sessionTimeoutMs(5000)
                .connectionTimeoutMs(5000)
                .retryPolicy(retryPolicy)
                .build();

        //启动客户端
        curatorFramework.start();

        boolean isZkCuratorStarted = curatorFramework.isStarted();

        logger.info("当前客户端的状态：" + (isZkCuratorStarted ? "已连接" : "已断开"));

        if (curatorFramework.checkExists().forPath(ZK_PATH) == null) {
            curatorFramework.create()
                    .creatingParentsIfNeeded()
                    .withMode(CreateMode.PERSISTENT)
                    .withACL(ZooDefs.Ids.OPEN_ACL_UNSAFE)
                    .forPath(ZK_PATH);
        }

        curatorFramework.getData().usingWatcher(myCuratorWatcher).forPath(ZK_PATH);

        return curatorFramework;
    }
}
