package com.wiley.zookeeper;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.NodeCache;
import org.apache.curator.framework.recipes.cache.NodeCacheListener;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.locks.InterProcessLock;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.zookeeper.CreateMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * @ClassName DistributedLock01
 * @Description TODO
 * @Author Rui.Wen
 * @Date 2020-03-31 20:33
 * @Version 1.0
 */

@Service
public class DistributedLock01 {

    private static Logger logger = LoggerFactory.getLogger(DistributedLock01.class);

    private final static String PARENT_PATH = "/zk_lock";
    private final static String SUB_PATH = "/lock_";


    private final static Integer THREAD_NUM = 5;

    private CountDownLatch countDownLatch = new CountDownLatch(1);
    Semaphore semaphore = new Semaphore(1);


    @Autowired
    private CuratorFramework curatorFramework;

    public void tryLock() {
        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(2);
        List<Future<String>> futureList = new ArrayList<>();
        for (int i = 0; i < THREAD_NUM; i++) {
            futureList.add(newFixedThreadPool.submit(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    // 创建临时有序的子节点
                    String myPath = curatorFramework.create().creatingParentsIfNeeded().
                            withMode(CreateMode.EPHEMERAL_SEQUENTIAL).forPath(PARENT_PATH + SUB_PATH)
                            .substring(PARENT_PATH.length() + 1);
                    return myPath;
                }
            }));
        }


        try {
            Thread.sleep(100);
            // 获取所有的字节点并排序
            List<String> pathList = curatorFramework.getChildren().forPath(PARENT_PATH);
            Collections.sort(pathList);
            logger.info("所有子节点{}", pathList);

            for (int i = 0; i < THREAD_NUM; i++) {
                int finalI = i;
                new Thread(() -> {
                    String myPath;
                    try {
                        myPath = futureList.get(finalI).get();
                        int index = pathList.indexOf(myPath);
                        if (index != 0) {
                            // 如果创建的子节点不是最小的，就监听比他小1位的节点
                            // 如果比他小1位的节点移除（释放锁），那么他获取锁
                            addWatcher(pathList.get(index - 1));
//                            countDownLatch.await();
                            handler_unlock(myPath);
                            semaphore.release();
                        } else {
                            semaphore.acquire();
                            handler_unlock(myPath);
                            semaphore.release();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            newFixedThreadPool.shutdown();
        }
    }

    private synchronized void handler_unlock(String myPath) throws Exception {
        logger.info("当前节点{}得到锁，开始处理业务~~~", myPath);
        Thread.sleep(3000);
        curatorFramework.delete().forPath(PARENT_PATH + "/" + myPath);
        logger.info("当前节点{}已删除（释放锁）", myPath);
//        if (countDownLatch.getCount() <= 0) {
//            countDownLatch = new CountDownLatch(1);
//        }
    }


    public void addWatcher(String path) throws Exception {
        logger.info("监听比当前节点小1位的节点{}", path);
        NodeCache nodeCache = new NodeCache(curatorFramework, PARENT_PATH + "/" + path, false);
        nodeCache.start();
        nodeCache.getListenable().addListener(new NodeCacheListener() {
            @Override
            public void nodeChanged() throws InterruptedException {
                String oldPath = nodeCache.getCurrentData().getPath();
                logger.info("上一个会话已释放锁或已断开...节点路径为: {}", oldPath);
                if (oldPath.contains(path)) {
//                    countDownLatch.countDown();
                    semaphore.acquire();
                }
            }
        });
    }

}
