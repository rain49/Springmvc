package com.wiley.zookeeper;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.api.CuratorWatcher;
import org.apache.curator.framework.recipes.cache.NodeCache;
import org.apache.zookeeper.WatchedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * @ClassName MyCuratorWatcher
 * @Description TODO
 * @Author Rui.Wen
 * @Date 2019-10-24 09:43
 * @Version 1.0
 */
@Component
public class MyCuratorWatcher implements CuratorWatcher {

    private static Logger logger = LoggerFactory.getLogger(MyCuratorWatcher.class);


    String zooDataPath = "/trip";

    @Lazy
    @Autowired
    private CuratorFramework curatorFramework;

    @Override
    public void process(WatchedEvent event) {

        logger.info("触发 watcher，节点路径为：" + event.getPath());

        try {
            final NodeCache nodeCache = new NodeCache(curatorFramework, zooDataPath);
            //建立Cache
            //该方法有个boolean类型的参数，默认是false，如果设置为true，那么NodeCache在第一次启动的时候就会立刻从ZooKeeper上读取对应节点的数据内容，并保存在Cache中。
            nodeCache.start(true);
            if (nodeCache.getCurrentData() != null) {
                logger.info("节点初始化数据为：" + new String(nodeCache.getCurrentData().getData()));
            } else {
                logger.debug("节点数据为空！");
            }

            nodeCache.getListenable().addListener(() -> {
                if (nodeCache.getCurrentData() == null) {
                    logger.debug("节点数据为空！");
                    return;
                }
                String data = new String(nodeCache.getCurrentData().getData());
                logger.info("节点路径：" + nodeCache.getCurrentData().getPath() + " 修改后的数据：" + data);
            });
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }
}
