package com.wiley.zookeeper;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;

import java.io.IOException;
import java.util.List;

/**
 * @ClassName ZkState
 * @Description zookeeper连接测试
 * @Author Rui.Wen
 * @Date 2019-07-19 14:00
 * @Version 1.0
 */
public class ZkState {

    public static void main(String[] args) throws IOException, KeeperException, InterruptedException {
        String hostPort = "10.86.216.178:2181";
        String zPath = "/";
        // 原生
        ZooKeeper zooKeeper = new ZooKeeper(hostPort, 2000, null);

        List<String> children = zooKeeper.getChildren(zPath, false);

        for (String path : children) {
            System.out.println("zPath======>" + path);
        }


        // 创建连接对象
        CuratorFramework cf = CuratorFrameworkFactory.builder()
                .connectString(hostPort)
                .sessionTimeoutMs(2000)
                .retryPolicy(new ExponentialBackoffRetry(1000,3))
                .build();
        cf.start();

        try {
            // 创建节点，无节点数据
            cf.create().forPath("/curator");
            // 创建节点和节点数据
            cf.create().forPath("/curator1","init".getBytes());
            // 创建临时节点，当前会话有效
            cf.create().withMode(CreateMode.EPHEMERAL).forPath("/ephemeral");
            // 创建一个临时节点，并自动创建父节点，这个接口非常有用，开发人员经常碰到NoNodeException
            cf.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT).forPath("/pEhe/111","临时父节点".getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
