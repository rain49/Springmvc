package com.wiley.service;

public interface CalculateDiscountService {

    String type();

    Double discount(Double price);
}
