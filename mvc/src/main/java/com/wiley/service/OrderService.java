package com.wiley.service;

import com.alibaba.druid.util.StringUtils;
import com.wiley.common.BaseResponse;
import com.wiley.dao.OrderDao;
import com.wiley.entity.ObOrder;
import com.wiley.event.OrderEvent;
import com.wiley.exception.ErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * 订单服务实现类
 */
@Service
public class OrderService {

    private static Logger logger = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    ApplicationContext applicationContext;
    @Autowired
    private OrderDao orderDao;

    /**
     * 获取用户订单
     *
     * @param name 用户名
     * @return
     * @throws ErrorException
     */
    public BaseResponse<List<ObOrder>> getAll(String name) throws ErrorException {

        BaseResponse response = new BaseResponse();
        if (StringUtils.isEmpty(name)) {
            logger.error("姓名不能为空");
            throw new ErrorException("姓名不能为空", "N500");
        }
        List<ObOrder> allOrders = orderDao.getAllOrders(name);
        if (!CollectionUtils.isEmpty(allOrders)) {
            /**
             * 观察者模式
             */
            // 发布事件（可同步，可异步）
            applicationContext.publishEvent(new OrderEvent(allOrders));
            response.setResult(allOrders);
        }

//        CyclicBarrier cyclicBarrier = new CyclicBarrier(100);
//        try {
//            cyclicBarrier.await();
//        } catch (InterruptedException | BrokenBarrierException e) {
//            e.printStackTrace();
//        }

        return response;
    }
}
