package com.wiley.service;

import org.springframework.stereotype.Component;

@Component
public class VipDiscount implements CalculateDiscountService {

    private static final String USER_TYPE = "VIP";

    @Override
    public String type() {
        return USER_TYPE;
    }

    @Override
    public Double discount(Double price) {
        return price * 0.8;
    }
}
