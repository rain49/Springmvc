package com.wiley.service;

import org.springframework.stereotype.Component;

@Component
public class NormalDiscount implements CalculateDiscountService {

    private static final String USER_TYPE = "NORMAL";

    @Override
    public String type() {
        return USER_TYPE;
    }

    @Override
    public Double discount(Double price) {
        return price * 0.9;
    }
}
