package com.wiley.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class CalculateDisCount {

    public Double calculateDiscount(String type, Double price) {
        return discountServiceHashMap.get(type).discount(price);
    }


    HashMap<String, CalculateDiscountService> discountServiceHashMap = new HashMap<>();

    // 工厂模式
    public CalculateDisCount(List<CalculateDiscountService> calculateDiscountServices) {
        System.out.println("-----------------------");
        for (CalculateDiscountService calculateDiscountService : calculateDiscountServices) {
            discountServiceHashMap.put(calculateDiscountService.type(), calculateDiscountService);
        }
    }

}
