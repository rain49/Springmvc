package com.wiley.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ObOrder implements Serializable {
    /**
     * 订单id
     **/
    private Long id;
    /**
     * 订单编号
     **/
    private String orderSeq;
    /**
     * 预定方式:1 app,2 web,3 TMC-白屏, 4 TMC-手工，5 TMC-PNR
     **/
    private String orderResource;
    /**
     * 订单类型:1 订票订单;2 酒店 ;一个父订单包含多个逗号隔开
     **/
    private String orderType;
    /**
     * 场景id
     **/
    private Long sceneId;
    /**
     * 场景名字
     **/
    private String sceneName;
    /**
     * 审批流程id
     **/
    private Long approvalConfigId;
    /**
     * 审批状态: 0待审批;1审批中;2审批通过;3审批不通过
     **/
    private String approverStatus;
    /**
     * 预定人code
     **/
    private String bookCode;
    /**
     * 订单归属人code
     **/
    private String orderPersonCode;
    /**
     * 订票人名字
     **/
    private String orderPersonName;
    /**
     * 补差支付方式: alipay:支付宝手机支付 wx:微信支付
     **/
    private String payChannel;
    /**
     * 补差总支付金额
     **/
    private BigDecimal personPrice;
    /**
     * 支付方式: 0:企业支付 1:企业+个人支付
     **/
    private String payType;
    /**
     * 支付时间
     **/
    private Date payTime;
    /**
     * 成本总价
     **/
    private BigDecimal baseTotalPrice;
    /**
     * 总价(实付金额)
     **/
    private BigDecimal totalPrice;
    /**
     * 订单状态:0:待付款,1:待审核,2:待出票,3:已出票,4 已改签 ,5 部分改签 ,6已退票,7部分退票 ,99订单关闭
     **/
    private String orderStatus;
    /**
     * 订单状态
     **/
    private String orderStatusCn;
    /**
     * 订单关闭原因
     **/
    private String orderCloseReason;
    /**
     * 订单备注信息
     **/
    private String remark;
    /**
     * 取消原因
     **/
    private String cancelReason;
    /**
     * 取消备注
     **/
    private String cancelRemark;
    /**
     * 创建时间
     **/
    private Date createTime;
    /**
     * 用户使用备注
     **/
    private String userRemark;
    /**
     * 订单超时时间
     **/
    private Integer timeOut;
    /**
     * 公司编码
     **/
    private String companyCode;
    /**
     * 公司名字
     **/
    private String companyName;
    /**
     * 成本中心code
     **/
    private String costCenterCode;
    /**
     * 是否代订
     **/
    private String bookAgent;
    /**
     * 通知人json
     **/
    private String noticePerson;
    /**
     * 短信通知人json
     **/
    private String smsReception;
    /**
     * 发起审批选中的节点数据
     **/
    private String startApproveLevel;
    /**
     * 当前审批id
     **/
    private Long approvalId;
    /**
     * 创建者的userCode
     **/
    private String createrCode;
    /**
     * 成本中心code
     **/
    private String costCenterName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderSeq() {
        return orderSeq;
    }

    public void setOrderSeq(String orderSeq) {
        this.orderSeq = orderSeq;
    }

    public String getOrderResource() {
        return orderResource;
    }

    public void setOrderResource(String orderResource) {
        this.orderResource = orderResource;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Long getSceneId() {
        return sceneId;
    }

    public void setSceneId(Long sceneId) {
        this.sceneId = sceneId;
    }

    public String getSceneName() {
        return sceneName;
    }

    public void setSceneName(String sceneName) {
        this.sceneName = sceneName;
    }

    public Long getApprovalConfigId() {
        return approvalConfigId;
    }

    public void setApprovalConfigId(Long approvalConfigId) {
        this.approvalConfigId = approvalConfigId;
    }

    public String getApproverStatus() {
        return approverStatus;
    }

    public void setApproverStatus(String approverStatus) {
        this.approverStatus = approverStatus;
    }

    public String getBookCode() {
        return bookCode;
    }

    public void setBookCode(String bookCode) {
        this.bookCode = bookCode;
    }

    public String getOrderPersonCode() {
        return orderPersonCode;
    }

    public void setOrderPersonCode(String orderPersonCode) {
        this.orderPersonCode = orderPersonCode;
    }

    public String getOrderPersonName() {
        return orderPersonName;
    }

    public void setOrderPersonName(String orderPersonName) {
        this.orderPersonName = orderPersonName;
    }

    public String getPayChannel() {
        return payChannel;
    }

    public void setPayChannel(String payChannel) {
        this.payChannel = payChannel;
    }

    public BigDecimal getPersonPrice() {
        return personPrice;
    }

    public void setPersonPrice(BigDecimal personPrice) {
        this.personPrice = personPrice;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public BigDecimal getBaseTotalPrice() {
        return baseTotalPrice;
    }

    public void setBaseTotalPrice(BigDecimal baseTotalPrice) {
        this.baseTotalPrice = baseTotalPrice;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatusCn() {
        return orderStatusCn;
    }

    public void setOrderStatusCn(String orderStatusCn) {
        this.orderStatusCn = orderStatusCn;
    }

    public String getOrderCloseReason() {
        return orderCloseReason;
    }

    public void setOrderCloseReason(String orderCloseReason) {
        this.orderCloseReason = orderCloseReason;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getCancelRemark() {
        return cancelRemark;
    }

    public void setCancelRemark(String cancelRemark) {
        this.cancelRemark = cancelRemark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUserRemark() {
        return userRemark;
    }

    public void setUserRemark(String userRemark) {
        this.userRemark = userRemark;
    }

    public Integer getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Integer timeOut) {
        this.timeOut = timeOut;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCostCenterCode() {
        return costCenterCode;
    }

    public void setCostCenterCode(String costCenterCode) {
        this.costCenterCode = costCenterCode;
    }

    public String getBookAgent() {
        return bookAgent;
    }

    public void setBookAgent(String bookAgent) {
        this.bookAgent = bookAgent;
    }

    public String getNoticePerson() {
        return noticePerson;
    }

    public void setNoticePerson(String noticePerson) {
        this.noticePerson = noticePerson;
    }

    public String getSmsReception() {
        return smsReception;
    }

    public void setSmsReception(String smsReception) {
        this.smsReception = smsReception;
    }

    public String getStartApproveLevel() {
        return startApproveLevel;
    }

    public void setStartApproveLevel(String startApproveLevel) {
        this.startApproveLevel = startApproveLevel;
    }

    public Long getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(Long approvalId) {
        this.approvalId = approvalId;
    }

    public String getCreaterCode() {
        return createrCode;
    }

    public void setCreaterCode(String createrCode) {
        this.createrCode = createrCode;
    }

    public String getCostCenterName() {
        return costCenterName;
    }

    public void setCostCenterName(String costCenterName) {
        this.costCenterName = costCenterName;
    }
}
