package com.wiley.common;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @ClassName BaseTest
 * @Description TODO
 * @Author Rui.Wen
 * @Date 2019-11-08 16:50
 * @Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:mvc-dispatcher-servlet.xml","classpath:mybatis-config.xml"})
@WebAppConfiguration("src/main/webapp/WEB-INF")
public class BaseTest {
}
