package com.wiley.common;

import com.wiley.exception.ErrorException;

/**
 * 统一返回数据结构
 *
 * @param <T> 返回数据
 * @author Rui.Wen
 * @date 2019/06/05
 */
public class BaseResponse<T> {

    private static final String sCode = "200";
    private static final String sMsg = "操作成功";
    private String message;

    private String code;

    private T result;

    public BaseResponse() {
        this.code = sCode;
        this.message = sMsg;
    }

    public BaseResponse(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 执行成功时构造方法
     *
     * @param result 执行结果
     */
    public BaseResponse(T result) {
        this.code = sCode;
        this.message = sMsg;
        this.result = result;
    }

    /**
     * 执行成功时构造方法
     *
     * @param result  执行结果
     * @param message 提示信息
     */
    public BaseResponse(T result, String message) {
        this.code = sCode;
        this.message = message;
        this.result = result;
    }

    /**
     * 执行异常时构造方法
     *
     * @param e 异常信息
     */
    public BaseResponse(ErrorException e) {
        this.message = e.getMessage();
        this.code = e.getErrorCode();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public boolean isSuccess() {
        return this.code != null && sCode.equals(this.code);
    }
}
