package com.wiley.proxy.dynamic.cglib;

import com.wiley.proxy.staticproxy.Student;
import org.springframework.cglib.proxy.Enhancer;

/**
 * @ClassName CglibProxyTest
 * @Description TODO
 * @Author Rui.Wen
 * @Date 2020-04-21 15:39
 * @Version 1.0
 */
public class CglibProxyTest {

    public static void main(String[] args) {

        // 创建Enhancer对象，类似于jdk动态代理的Proxy类
        Enhancer enhancer = new Enhancer();

        // 设置目标类的class对象
        enhancer.setSuperclass(Student.class);

        // 实质回调函数
        enhancer.setCallback(new MyMethodInterceptor());

        // 创建代理类
        Student monitor = (Student) enhancer.create();
        monitor.setName("james");
        monitor.giveMoney();
    }
}
