package com.wiley.proxy.dynamic.cglib;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @ClassName MyMethodInterceptor
 * @Description 自定义方法拦截器
 * @Author Rui.Wen
 * @Date 2020-04-21 15:36
 * @Version 1.0
 */
public class MyMethodInterceptor implements MethodInterceptor {

    /**
     * @param o           增强的对象，也就是实现这个接口的类的一个对象
     * @param method      被拦截的方法
     * @param objects     被拦截的方法的参数
     * @param methodProxy 触发父类的方法对象
     * @return
     * @throws Throwable
     */
    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        return methodProxy.invokeSuper(o, objects);
    }
}
