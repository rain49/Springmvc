package com.wiley.proxy.dynamic.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @ClassName StuInvocationHandler
 * @Description 自定义代理类执行处理器
 * @Author Rui.Wen
 * @Date 2020-04-21 14:10
 * @Version 1.0
 */
public class StuInvocationHandler<T> implements InvocationHandler {

    // invocationHandler持有的被代理对象
    T target;

    public StuInvocationHandler(T target) {
        this.target = target;
    }

    /**
     * @param proxy  代理对象
     * @param method 执行方法
     * @param args   执行方法的参数
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        before(method);
        Object invoke = method.invoke(target, args);
        after(method);
        return invoke;
    }

    private void before(Method method) {
        System.out.println("开始代理执行" + method.getName() + "方法");
    }


    private void after(Method method) {
        System.out.println("结束代理执行" + method.getName() + "方法");
    }
}
