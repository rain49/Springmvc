package com.wiley.proxy.dynamic.jdk;

import com.wiley.proxy.staticproxy.Monitor;
import com.wiley.proxy.staticproxy.Student;

import java.lang.reflect.Proxy;

/**
 * @ClassName ProxyTest
 * @Description TODO
 * @Author Rui.Wen
 * @Date 2020-04-21 14:35
 * @Version 1.0
 */
public class ProxyTest {
    public static void main(String[] args) {
        // 创建被代理类
        Monitor student = new Student("rose");
        // 创建被代理类相应的执行处理器
        StuInvocationHandler<Monitor> invocationHandler = new StuInvocationHandler<>(student);
        // 创建代理对象，代理对象的每个执行方法都会替换执行Invocation中的invoke方法
        Monitor proxyInstance = (Monitor) Proxy.newProxyInstance(
                Monitor.class.getClassLoader(),
                new Class<?>[]{Monitor.class},
                invocationHandler);
        proxyInstance.giveMoney();


        // 静态代理，代理类是手动new出来的
    }
}
