package com.wiley.proxy.staticproxy;

/**
 * @ClassName StudentProxy
 * @Description 代理类
 * @Author Rui.Wen
 * @Date 2020-04-21 09:32
 * @Version 1.0
 */
public class StudentProxy implements Monitor {

    // 被代理类
    Student stu;

    public StudentProxy(Monitor stu) {
        if (stu.getClass() == Student.class) {
            this.stu = (Student) stu;
        }
    }

    @Override
    public void giveMoney() {
        System.out.println("开始交班费了！！！");
        stu.giveMoney();
        System.out.println(stu.getName() + "是高级");
    }
}
