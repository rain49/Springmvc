package com.wiley.proxy.staticproxy;

/**
 * @ClassName StaticProxyTest
 * @Description 静态代理测试
 * @Author Rui.Wen
 * @Date 2020-04-21 09:34
 * @Version 1.0
 */
public class StaticProxyTest {

    public static void main(String[] args) {
        // 被代理对象
        Monitor monitor = new Student("kobe");

        // 代理对象
        Monitor monitorProxy = new StudentProxy(monitor);
        monitorProxy.giveMoney();
    }
}
