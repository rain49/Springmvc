package com.wiley.proxy.staticproxy;

/**
 * @ClassName Student
 * @Description 被代理类
 * @Author Rui.Wen
 * @Date 2020-04-21 09:30
 * @Version 1.0
 */
public class Student implements Monitor {

    private String name;

    public Student() {
    }

    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void giveMoney() {
        System.out.println(name + "上交班费50元");
    }
}
