package com.wiley.proxy.staticproxy;

/**
 * @ClassName Monitor
 * @Description 公共接口
 * @Author Rui.Wen
 * @Date 2020-04-21 09:28
 * @Version 1.0
 */
public interface Monitor {

    /**
     * 交班费
     */
    void giveMoney();
}
