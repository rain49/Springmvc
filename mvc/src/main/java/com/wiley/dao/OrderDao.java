package com.wiley.dao;

import com.wiley.entity.ObOrder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDao {

    List<ObOrder> getAllOrders(String name);
}
