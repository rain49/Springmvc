package com.wiley.reflect;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * file：Reflect_01.java
 * description: 反射实现的三种方式
 * date: 2020-04-29 15:11
 * author: wenrui
 * version: 1.0
 */
public class Reflect_01 {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException, CloneNotSupportedException {
        // 方式1：通过类的实例获取 创建对象阶段
        User user = new User();
        Class userClass1 = user.getClass();
        String className1 = userClass1.getName();
        System.out.println(className1);  // com.wiley.entity.ObOrder

        // 方式2：通过相对路径获取 源文件阶段
        Class<?> userClass2 = Class.forName("com.wiley.reflect.User");
        String className2 = userClass2.getName();
        System.out.println(className2);

        // 方式2：通过类名获取 字节码阶段
        Class<User> userClass3 = User.class;
        String className3 = userClass3.getName();
        System.out.println(className3);

        // 通过构造器创建对象
        // 1.无参构造器
        Constructor<User> constructor = userClass3.getConstructor();
        User user1 = constructor.newInstance();
        // 2.有参构造器
        Constructor<User> constructor1 = userClass3.getConstructor(
                String.class,
                String.class,
                Integer.class);
        User user2 = constructor1.newInstance("kobe", "male", 40);
        System.out.println(ToStringBuilder.reflectionToString(user2, ToStringStyle.JSON_STYLE));

        // 获取全部构造器
        Constructor<?>[] constructors = userClass3.getConstructors();
        for (int i = 0; i < constructors.length; i++) {
            Class<?>[] parameterTypes = constructors[i].getParameterTypes();
            System.out.println("第" + i + "个构造函数");
            for (Class<?> parameterType : parameterTypes) {
                System.out.println(parameterType.getName());
            }
        }

        // Field
        Field name = userClass3.getDeclaredField("name");
        name.setAccessible(true);
        name.set(user2, "rose");
        System.out.println(ToStringBuilder.reflectionToString(user2, ToStringStyle.JSON_STYLE));

        // Method
        Method toString = userClass3.getMethod("toString");
        toString.setAccessible(true);
        System.out.println(toString.invoke(user2));

        Object clone = user2.clone();
        System.out.println(ToStringBuilder.reflectionToString(clone));
    }
}
