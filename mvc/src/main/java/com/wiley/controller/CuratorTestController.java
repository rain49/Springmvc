package com.wiley.controller;

import com.wiley.zookeeper.DistributedLock01;
import com.wiley.zookeeper.DistributedLockByCurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @ClassName TestController 测试控制类
 * @Description TODO
 * @Author Rui.Wen
 * @Date 2019-11-08 17:12
 * @Version 1.0
 */
@RestController
@RequestMapping("/test")
public class CuratorTestController {

    private static Logger logger = LoggerFactory.getLogger(CuratorTestController.class);

    @Autowired
    private DistributedLockByCurator distributedLockByCurator;
    @Autowired
    private DistributedLock01 distributedLock01;

    /**
     * 测试分布式锁
     *
     * @return
     */
    @GetMapping("/lock")
    public void lock() {
        String path = "test";
        final ExecutorService threadPool = Executors.newCachedThreadPool();
        logger.info("开始购买...");
        for (int i = 0; i < 5; i++) {
            threadPool.execute(() -> {
                logger.info("线程[" + Thread.currentThread().getName() + "]开始抢购!!!");
                distributedLockByCurator.acquireLock(path);
                logger.info("线程[" + Thread.currentThread().getName() + "]正在购买中...");
                try {
                    // 提交订单，减库存
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                logger.info("线程[" + Thread.currentThread().getName() + "]购买结束,下一位-->");

                try {
                    distributedLockByCurator.releaseLock(path);
                    logger.info("释放完毕...");
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                    distributedLockByCurator.releaseLock(path);
                }
            });
        }

        threadPool.shutdown();
    }


    @GetMapping("/lock1")
    public void lock1() {
        distributedLock01.tryLock();
    }
}
