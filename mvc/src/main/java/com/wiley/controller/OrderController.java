package com.wiley.controller;

import com.wiley.common.BaseResponse;
import com.wiley.entity.ObOrder;
import com.wiley.exception.ErrorException;
import com.wiley.service.CalculateDisCount;
import com.wiley.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {
    private static Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;
    @Autowired
    CalculateDisCount calculateDisCount;

    /**
     * 获取用户订单
     *
     * @param name 用户名
     * @return
     * @throws ErrorException
     */
    @GetMapping("/getAll")
    public BaseResponse<List<ObOrder>> getAll(String name) throws ErrorException {
        logger.info("获取订单入参：{}", name);
        return orderService.getAll(name);
    }

    @PutMapping("/discount/{type}/{price}")
    public BaseResponse<Double> calculateDiscount(@PathVariable(name = "type") String type,
                                                  @PathVariable(name = "price") Double price) {
        logger.info("计算折扣入参：{}，{}", type, price);
        Double discount = calculateDisCount.calculateDiscount(type, price);
        return new BaseResponse<>(discount);
    }
}
