package com.wiley.controller;

import com.alibaba.fastjson.JSON;
import com.wiley.common.BaseResponse;
import com.wiley.rocketmq.MqProducerConfig;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;

/**
 * @ClassName MqController
 * @Description TODO
 * @Author Rui.Wen
 * @Date 2019-11-25 16:33
 * @Version 1.0
 */
@RestController
@RequestMapping("/mq")
public class MqController {

    private static Logger logger = LoggerFactory.getLogger(MqController.class);

    //    @Autowired
//    private DefaultMQProducer defaultMQProducer;
    @Autowired
    private TransactionMQProducer transactionMQProducer;

//    @GetMapping("/send")
//    public BaseResponse sendMsg(String msg) {
//        SendResult sendResult = null;
//
//        try {
//            Message message = new Message("testTopic", "testTag", msg.getBytes("UTF-8"));
//            sendResult = defaultMQProducer.send(message);
//            logger.info("消息发送结果：{}", JSON.toJSONString(sendResult));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return new BaseResponse(sendResult.getSendStatus());
//    }

    @GetMapping("/trans")
    public BaseResponse sendTransactionMsg(String msg) {
        logger.info("请求参数：{}", msg);

        SendResult sendResult = null;

        try {
            for (int i = 0; i < 2; i++) {
                Message message = new Message("transactionTopic", "transaction", "key" + i + 1, (msg + i).getBytes("UTF-8"));
                sendResult = transactionMQProducer.sendMessageInTransaction(message, new Object());
                logger.info("事务消息发送结果：{}", JSON.toJSONString(sendResult));
            }
        } catch (UnsupportedEncodingException | MQClientException e) {
            e.printStackTrace();
        }

        return new BaseResponse(sendResult.getSendStatus());
    }

}
