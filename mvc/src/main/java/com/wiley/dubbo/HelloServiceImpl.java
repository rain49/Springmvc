package com.wiley.dubbo;

import com.alibaba.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * file：HelloServiceImpl.java
 * description:
 * date: 2020-05-26 16:47
 * author: wenrui
 * version: 1.0
 */
@Service //使用dubbo提供的service注解，注册暴露服务
public class HelloServiceImpl implements HelloService {

    private static Logger logger = LoggerFactory.getLogger(HelloServiceImpl.class);

    @Override
    public String sayHello(String name) {
        String hello = "hello" + name + "!!";
        logger.info(hello);
        return hello;
    }
}
