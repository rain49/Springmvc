package com.wiley.dubbo;

/**
 * file：HelloService.java
 * description:对外声明服务的接口
 * date: 2020-05-26 16:47
 * author: wenrui
 * version: 1.0
 */
public interface HelloService {
    String sayHello(String name);
}
