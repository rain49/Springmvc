package com.wiley.AOP;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.concurrent.locks.Lock;

/**
 * @ClassName MyAspect
 * @Description 切面类
 * @Author Rui.Wen
 * @Date 2020-04-21 16:58
 * @Version 1.0
 */
@Component
@Aspect
public class MyAspect {

    // 第一个*表示匹配任意的方法返回值，..(两个点)表示零个或多个，
    // 上面的第一个..表示service包及其子包,第二个*表示所有类,
    // 第三个*表示所有方法，第二个..表示方法的任意参数个数
    @Pointcut("execution(* com.wiley.controller..*.*(..))")
    public void pointCut() {
    }

    @Before("pointCut()")
    public void doBefore(JoinPoint joinPoint) {
        System.out.println("AOP Before Advice..." + joinPoint);
    }

    @After("pointCut()")
    public void doAfter(JoinPoint joinPoint) {
        System.out.println("AOP After Advice...");
    }

    @AfterReturning(pointcut = "pointCut()", returning = "returnVal")
    public void afterReturn(JoinPoint joinPoint, Object returnVal) {
        System.out.println("AOP AfterReturning Advice:" + returnVal);
    }

    @AfterThrowing(pointcut = "pointCut()", throwing = "error")
    public void afterThrowing(JoinPoint joinPoint, Throwable error) {
        System.out.println("AOP AfterThrowing Advice..." + error);
        System.out.println("AfterThrowing...");
    }

    @Around("pointCut()")
    public void around(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("AOP Around before...");

        pjp.proceed();

        System.out.println("AOP Around after...");

    }

}
