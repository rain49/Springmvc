package com.wiley.event;

import org.springframework.context.ApplicationEvent;

/**
 * 订单事件
 */
public class OrderEvent extends ApplicationEvent {

    public OrderEvent(Object source) {
        super(source);
    }
}
