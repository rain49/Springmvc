package com.wiley.listener;

import com.alibaba.fastjson.JSON;
import com.wiley.event.OrderEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * 监听订单事件，收到通知后发送短信
 */
@Component
public class OrderEventListener implements ApplicationListener<OrderEvent> {

    private static Logger logger = LoggerFactory.getLogger(OrderEventListener.class);

    @Override
    public void onApplicationEvent(OrderEvent orderEvent) {
        logger.info("监听到的数据========={}", JSON.toJSONString(orderEvent.getSource()));
    }
}
