package com.wiley.rocketmq;

import org.apache.rocketmq.client.producer.LocalTransactionState;
import org.apache.rocketmq.client.producer.TransactionListener;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;

/**
 * @ClassName TransactionCheckListenerImpl
 * @Description priobroker回查producer监听
 * @Author Rui.Wen
 * @Date 2019-11-27 15:31
 * @Version 1.0
 */
@Service
public class TransactionCheckListenerImpl implements TransactionListener {

    private static Logger logger = LoggerFactory.getLogger(TransactionCheckListenerImpl.class);

    @Override
    public LocalTransactionState executeLocalTransaction(Message message, Object o) {

        try {
            logger.info("producer发送的事务消息：{}", new String(message.getBody(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return LocalTransactionState.ROLLBACK_MESSAGE;
        }

        // TODO 生产者操作本地事务

        if (message.getKeys().equals("key11")) {
            int i = 1 / 0;
        }

        logger.info("=================");
        return LocalTransactionState.COMMIT_MESSAGE;
    }

    @Override
    public LocalTransactionState checkLocalTransaction(MessageExt messageExt) {
        logger.info("未决事务，服务器端回查事务消息：{}" + messageExt);
        //由于RocketMQ迟迟没有收到消息的确认消息，因此主动询问这条prepare消息，是否正常？
        //可以查询数据库看这条数据是否已经处理
        // TODO
        return LocalTransactionState.COMMIT_MESSAGE;
    }
}
