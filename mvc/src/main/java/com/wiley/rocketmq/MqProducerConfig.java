package com.wiley.rocketmq;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName MqProducerConfig
 * @Description rocketmq生产者配置类
 * @Author Rui.Wen
 * @Date 2019-11-25 14:54
 * @Version 1.0
 */
@Configuration
public class MqProducerConfig {

    private static Logger logger = LoggerFactory.getLogger(MqProducerConfig.class);

    private static final String producerGroup = "testProducerGroup";
    private static final String namesrvAddr = "10.86.216.181:9876";

//    @Bean
    public DefaultMQProducer init() throws MQClientException {
        DefaultMQProducer defaultMQProducer = new DefaultMQProducer(producerGroup);
        defaultMQProducer.setNamesrvAddr(namesrvAddr);
        defaultMQProducer.setRetryTimesWhenSendFailed(3);
        defaultMQProducer.start();

        logger.info("rocketMQ producer is started!!!");

        return defaultMQProducer;
    }
}
