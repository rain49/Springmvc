package com.wiley.rocketmq;

import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * @ClassName TestMqTransactionConsumer
 * @Description 事务消费着
 * @Author Rui.Wen
 * @Date 2019-11-27 15:40
 * @Version 1.0
 */
@Service
public class TestMqTransactionConsumer extends MqConsumerConfig implements ApplicationListener<ContextRefreshedEvent> {

    private static Logger logger = LoggerFactory.getLogger(TestMqTransactionConsumer.class);

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        try {
            super.listener("transactionTopic", "*", "transaction");
        } catch (MQClientException e) {
            logger.error("消费者监听器启动失败", e);
        }
    }

    @Override
    public ConsumeConcurrentlyStatus dealMsg(List<MessageExt> msgs) {
        for (MessageExt msg : msgs) {
            try {
                String msgStr = new String(msg.getBody(), "UTF-8");
                logger.info("Transaction rocketmq 消费者接收到的消息：{}", msgStr);
            } catch (UnsupportedEncodingException e) {
                logger.error("body转字符串解析失败");
                return ConsumeConcurrentlyStatus.RECONSUME_LATER;
            }
        }
        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
    }
}
