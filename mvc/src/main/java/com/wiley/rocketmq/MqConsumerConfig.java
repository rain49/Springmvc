package com.wiley.rocketmq;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @ClassName MqConsumerConfig
 * @Description rocketmq 消费者配置类
 * @Author Rui.Wen
 * @Date 2019-11-26 13:32
 * @Version 1.0
 */
@Configuration
public abstract class MqConsumerConfig {

    private static Logger logger = LoggerFactory.getLogger(MqConsumerConfig.class);

    private static final String consumerGroup = "testConsumerGroup";
    private static final String namesrvAddr = "10.86.216.181:9876";

    public void listener(String topic, String tag, String instanceName) throws MQClientException {
        DefaultMQPushConsumer defaultMQPushConsumer = new DefaultMQPushConsumer(consumerGroup);
        defaultMQPushConsumer.setNamesrvAddr(namesrvAddr);
        defaultMQPushConsumer.subscribe(topic, tag);

        // 开启内部类实现监听
        defaultMQPushConsumer.registerMessageListener((MessageListenerConcurrently) (msgs, context) -> dealMsg(msgs));

        defaultMQPushConsumer.setInstanceName(instanceName);
        defaultMQPushConsumer.start();

        logger.info("rocketmq " + instanceName + " consumer is started!!!");
    }

    // 处理消息
    public abstract ConsumeConcurrentlyStatus dealMsg(List<MessageExt> msgs);
}
