package com.wiley.rocketmq;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName MqTransactionProducerConfig
 * @Description rocketmq 事务生产者配置类
 * @Author Rui.Wen
 * @Date 2019-11-26 15:30
 * @Version 1.0
 */
@Configuration
public class MqTransactionProducerConfig {

    private static Logger logger = LoggerFactory.getLogger(MqTransactionProducerConfig.class);

    private static final String producerGroup = "transactionProducerGroup";
    private static final String namesrvAddr = "10.86.216.181:9876";

    @Autowired
    private TransactionCheckListenerImpl transactionCheckListener;

    @Bean
    public TransactionMQProducer init() throws MQClientException {
//        TransactionListener transactionCheckListener = new TransactionCheckListenerImpl();
        // 构造事务消息的生产者
        TransactionMQProducer transactionMQProducer = new TransactionMQProducer(producerGroup);
        transactionMQProducer.setNamesrvAddr(namesrvAddr);
        transactionMQProducer.setRetryTimesWhenSendFailed(3);

        transactionMQProducer.setTransactionListener(transactionCheckListener);

        transactionMQProducer.start();

        logger.info("rocketMQ transaction producer is started!!!");

        return transactionMQProducer;
    }
}
