package com.wiley.exception;

import com.wiley.common.BaseResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class WebExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(WebExceptionHandler.class);

    /**
     * 处理业务异常
     *
     * @author Rui.Wen
     * @date 2019/06/05 15:00
     */
    @ExceptionHandler(ErrorException.class)
    @ResponseBody
    public BaseResponse errorException(ErrorException e) {
        logger.error(e.getMessage(), e);
        return new BaseResponse(e.getErrorCode(), e.getMessage());
    }

    /**
     * 处理系统异常
     *
     * @author Rui.Wen
     * @date 2019/06/05 15:00
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public BaseResponse handleException(Exception e) {
        if (e.getCause() != null) {
            logger.error(e.getCause().getMessage(), e);
        } else {
            logger.error(e.getMessage(), e);
        }
        return new BaseResponse("500", "服务器异常");
    }
}
