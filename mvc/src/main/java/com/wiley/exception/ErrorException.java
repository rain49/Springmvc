/**
 * @Title: BaseResponseException.java
 * @Package com.madiot.activiti.console.common
 * @Description: TODO
 * @author Yi.Wang2
 * @date 2018/10/8
 * @version
 */
package com.wiley.exception;

public class ErrorException extends Exception {

    private String errorCode;

    public ErrorException(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public ErrorException(String message, String errorCode, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public ErrorException(String errorCode, Throwable cause) {
        super(cause);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
