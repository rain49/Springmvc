package com.wiley.redis;

import com.wiley.common.BaseTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Map;

/**
 * @ClassName RedisBase
 * @Description redis 数据类型操作
 * @Author Rui.Wen
 * @Date 2020-05-05 20:07
 * @Version 1.0
 */
public class RedisBase extends BaseTest {

    @Autowired
    private JedisPool jedisPool;

    private Jedis jedis;

    @Before
    public void before() {
        jedis = jedisPool.getResource();
    }

    @After
    public void after() {
        jedisPool.close();
    }


    @Test
    public void string1() {
        String s = jedis.set("name", "wiley");
        System.out.println(s);
    }

    @Test
    public void hash() {
        Map<String, String> s = jedis.hgetAll("user1");
        System.out.println(s);
    }

    @Test
    public void list() {


    }
}
